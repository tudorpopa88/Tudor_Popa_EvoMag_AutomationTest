package com.evomag.automation.utils;

import java.sql.*;

public class SQLConnection {

    private Connection conn;
    private Statement statement;
    private ResultSet resultSet;

    public Connection getConn() {
        return conn;
    }

    public Statement getStatement() {
        return statement;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

//    public SQLConnection(Connection conn, Statement statement, ResultSet resultSet) {
//        this.conn = conn;
//        this.statement = statement;
//        this.resultSet = resultSet;
//    }

    public void connectToSQL(String selectQuery) throws SQLException {
        String host = "localhost";
        String port = "3306";
        conn = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/evomag_aut", "root", "delphi");
        statement = conn.createStatement();
        resultSet = statement.executeQuery(selectQuery);

        resultSet.close();
        conn.close();
    }
}
