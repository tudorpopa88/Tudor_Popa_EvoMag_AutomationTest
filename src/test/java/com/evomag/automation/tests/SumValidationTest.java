package com.evomag.automation.tests;

import com.evomag.automation.pages.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.sql.SQLException;

/**
 * Created by tudor on 11/3/2017.
 */
public class SumValidationTest extends BaseTest {

    /**This method is used to calculate the sum of the basket products and compare it with the final payment amount
     *
     * The steps are:
     * 1. Login to test account
     * 2. Select a random category of products
     * 3. Select multiple products and add to cart
     * 4. Validate the sum of the products from the shopping basket
     * 5. Logout
     *
     * @see LoginTestAccountPage for more details
     * @see CategoryRandomizerPage for more details
     * @see CartRandomizerPage for more details
     * @see CalculateProductsSumPage for more details
     * @see LogoutPage for more details
     * @throws SQLException
     */
    @Test
    public void sumValidationTest() {
        
        try {
            driver.manage().window().maximize();
            driver.get("https://www.evomag.ro/client/auth");
            LoginTestAccountPage loginTestAccount = PageFactory.initElements(driver, LoginTestAccountPage.class);
            CategoryRandomizerPage categoryRandomizer = PageFactory.initElements(driver, CategoryRandomizerPage.class);
            CartRandomizerPage productRandomizer = PageFactory.initElements(driver, CartRandomizerPage.class);
            CalculateProductsSumPage calculateProductsSum = PageFactory.initElements(driver, CalculateProductsSumPage.class);
            LogoutPage logout = PageFactory.initElements(driver, LogoutPage.class);
            loginTestAccount.loginTestAcc("marius.dragomir.50@gmail.com", "marius5050");
//        BypassCampaignPage access = PageFactory.initElements(driver, BypassCampaignPage.class);
//        access.bypassCampaign();
            Actions actions = new Actions(driver);
            categoryRandomizer.catRand(actions, driver);
            for (int i=0; i<3; i++) {
                productRandomizer.cartRand();
            }
            calculateProductsSum.calcProdSum();
            driver.navigate().to("https://www.evomag.ro");
            logout.logoutAccount(actions);
        } catch (Exception e) {
            System.out.println("The test encountered the following error " + e.getMessage());
            e.printStackTrace();
        }

    }
}
