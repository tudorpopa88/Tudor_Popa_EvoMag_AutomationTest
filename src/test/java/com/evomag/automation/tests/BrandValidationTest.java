package com.evomag.automation.tests;

import com.evomag.automation.pages.BrandValidationPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

/**
 * Created by tudor on 11/5/2017.
 */
public class BrandValidationTest extends BaseTest {

    /**This method is used to validate the manufacturer's name and number of products from a given category
     *
     * The steps are:
     * 1. Open the Laptops category
     * 2. Select a brand subcategory and validate its name
     * 3. Validate the number of products from the given subcategory
     *
     *@see BrandValidationPage fore more details
     */
    @Test
    public void brandValidationTest() {
        try {
            driver.manage().window().maximize();
            driver.get("https://www.evomag.ro");
            driver.navigate().to("https://www.evomag.ro/PORTABILE-Laptopuri/");
            BrandValidationPage brandValidation = PageFactory.initElements(driver, BrandValidationPage.class);
            brandValidation.brandNameVal(driver);
            brandValidation.brandNumberVal();
        } catch (Exception e) {
            System.out.println("The test encountered the following error " + e.getMessage());
            e.printStackTrace();
        }
    }
}
