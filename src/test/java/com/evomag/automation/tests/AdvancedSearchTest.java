package com.evomag.automation.tests;

import com.evomag.automation.pages.AdvancedSearchPage;
import com.evomag.automation.pages.CategoryRandomizerPage;
import com.evomag.automation.pages.LoginTestAccountPage;
import com.evomag.automation.pages.LogoutPage;
import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.sql.SQLException;

public class AdvancedSearchTest extends BaseTest{

    /**This method is used to test the advanced search feature
     *
     * The steps are:
     * 1. Login to test account
     * 2. Select a random category
     * 3. Open the advanced search page
     * 4. Apply the filters
     * 5. Logout
     *
     * @see LoginTestAccountPage for more details
     * @see CategoryRandomizerPage for more details
     * @see AdvancedSearchPage for more details
     * @see LogoutPage for more details
     * @throws SQLException
     */
    @Test
    public void advancedSearchTest() {
        try {
            driver.manage().window().maximize();
            driver.get("https://www.evomag.ro/client/auth");
            LoginTestAccountPage loginTestAccount = PageFactory.initElements(driver, LoginTestAccountPage.class);
            CategoryRandomizerPage categoryRandomizer = PageFactory.initElements(driver, CategoryRandomizerPage.class);
            AdvancedSearchPage advancedSearch = PageFactory.initElements(driver, AdvancedSearchPage.class);
            LogoutPage logout = PageFactory.initElements(driver, LogoutPage.class);
            Actions actions = new Actions(driver);
            loginTestAccount.loginTestAcc("marius.dragomir.50@gmail.com", "marius5050");
//        driver.navigate().to("https://www.evomag.ro/Componente-PC-Placi-Video/");
            categoryRandomizer.catRand(actions, driver);
            ThreadSleeper.mySleeper(2500L);
            advancedSearch.advSearch(driver, "100", "3000");

            categoryRandomizer.catRand(actions, driver);
            ThreadSleeper.mySleeper(2500L);
            advancedSearch.advSearch(driver, "2500", "50");
            advancedSearch.searchErrorAssert();

            categoryRandomizer.catRand(actions, driver);
            ThreadSleeper.mySleeper(2500L);
            advancedSearch.advSearch(driver, "osvjlwefij", "#&#)$&*");
            advancedSearch.searchErrorAssert();

            driver.navigate().to("https://www.evomag.ro");
            logout.logoutAccount(actions);
        } catch (Exception e) {
            System.out.println("The test encountered the following error " + e.getMessage());
            e.printStackTrace();
        }
    }
}
