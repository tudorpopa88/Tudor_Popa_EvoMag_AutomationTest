package com.evomag.automation.tests;

import com.evomag.automation.pages.CategoryRandomizerPage;
import com.evomag.automation.pages.PriceOrdererPage;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

/**
 * Created by tudor on 11/4/2017.
 */
public class PriceOrderValidationTest extends BaseTest {

    /**This method is used to test the order of the displayed products from a given category
     *
     * The steps are:
     * 1. Select a random category
     * 2. Sort the products after price on ascending order
     * 3. Validate the order of the displayed prices
     * 4. Sort the products after price on descending order
     * 5. Validate the order of the displayed prices
     *
     * @see CategoryRandomizerPage for more details
     * @see PriceOrdererPage for more details
     * @see CategoryRandomizerPage for more details
     */
    @Test
    public void priceOrderValTest() {
        try {
            driver.manage().window().maximize();
            driver.get("https://www.evomag.ro");
            CategoryRandomizerPage categoryRandomizer = PageFactory.initElements(driver, CategoryRandomizerPage.class);
            PriceOrdererPage priceOrderer = PageFactory.initElements(driver, PriceOrdererPage.class);
            Actions actions = new Actions(driver);
            categoryRandomizer.catRand(actions, driver);
            priceOrderer.priceOrdererAsc(driver);
            categoryRandomizer.catRand(actions, driver);
            priceOrderer.priceOrdererDesc(driver);
        } catch (Exception e) {
            System.out.println("The test encountered the following error " + e.getMessage());
            e.printStackTrace();
        }
    }
}
