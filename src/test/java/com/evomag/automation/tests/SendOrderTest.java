package com.evomag.automation.tests;

import com.evomag.automation.pages.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class SendOrderTest extends BaseTest {

    /**This method is used to test the order functionality
     *
     * The steps are:
     * 1. Login to test acccount
     * 2. Select a random category of products
     * 3. Add a random product to cart
     * 4. Open the billing page
     * 5. Complete the page with the order's specifications
     * 6. Logout
     *
     * @see LoginTestAccountPage for more details
     * @see CategoryRandomizerPage for more details
     * @see CartRandomizerPage for more details
     * @see LogoutPage for more details
     */
    @Test
    public void sendOrderTest() {
        try {
            driver.manage().window().maximize();
            driver.get("http://www.evomag.ro/client/auth");
            LoginTestAccountPage loginTestAccount = PageFactory.initElements(driver, LoginTestAccountPage.class);
            CategoryRandomizerPage categoryRandomizer = PageFactory.initElements(driver, CategoryRandomizerPage.class);
            CartRandomizerPage cartRandomizer = PageFactory.initElements(driver, CartRandomizerPage.class);
            BillingPage billing = PageFactory.initElements(driver, BillingPage.class);
            LogoutPage logout = PageFactory.initElements(driver, LogoutPage.class);
            loginTestAccount.loginTestAcc("marius.dragomir.50@gmail.com", "marius5050");
//            BypassCampaignPage access = PageFactory.initElements(driver, BypassCampaignPage.class);
//            access.bypassCampaign();
            Actions actions = new Actions(driver);
            for(int n=0; n<3; n++) {
                categoryRandomizer.catRand(actions, driver);
                cartRandomizer.cartRand();
            }
            billing.billing(driver);
            driver.navigate().to("https://www.evomag.ro");
            logout.logoutAccount(actions);
        } catch (Exception e) {
            System.out.println("The test encountered the following error " + e.getMessage());
            e.printStackTrace();
        }
    }
}
