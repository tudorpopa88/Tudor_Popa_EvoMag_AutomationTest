package com.evomag.automation.tests;

import com.evomag.automation.pages.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.sql.SQLException;

public class WishlistTest extends BaseTest {

    /**This method creates two wishlist's and validate their visibility like public or private
     *
     * The steps are:
     * 1. Login to test account
     * 2. Select a random category of products
     * 3. Select multiple products and add to cart
     * 4. Create a public wishlist with the selected products
     * 5. Select a random category of products
     * 6. Select multiple products and add to cart
     * 7. Create a private wishlist with the selected products
     * 8. Compare their visibility level
     * 9. Logout
     *
     * @see LoginTestAccountPage for more details
     * @see CategoryRandomizerPage for more details
     * @see CartRandomizerPage for more details
     * @see WishlistPage for more details
     * @see LogoutPage for more details
     * @throws SQLException
     */
    @Test
    public void wishlistTest() {
        try {
            driver.manage().window().maximize();
            driver.get("https://www.evomag.ro/client/auth");
            LoginTestAccountPage loginTestAccount = PageFactory.initElements(driver, LoginTestAccountPage.class);
            CategoryRandomizerPage categoryRandomizer = PageFactory.initElements(driver,CategoryRandomizerPage.class);
            CartRandomizerPage cartRandomizer = PageFactory.initElements(driver, CartRandomizerPage.class);
            WishlistPage wishlist = PageFactory.initElements(driver, WishlistPage.class);
            LogoutPage logout = PageFactory.initElements(driver, LogoutPage.class);
            Actions actions = new Actions(driver);
            loginTestAccount.loginTestAcc("marius.dragomir.50@gmail.com", "marius5050");
//        BypassCampaignPage access = PageFactory.initElements(driver, BypassCampaignPage.class);
//        access.bypassCampaign();
            categoryRandomizer.catRand(actions, driver);
            for(int i=0; i<3; i++) {
                cartRandomizer.cartRand();
            }
            driver.navigate().to("https://www.evomag.ro/cart/sendOrder");
            wishlist.wish(driver, "WishlistTest1", "This list will be public");
            wishlist.makePublic(driver);
            driver.navigate().to(("https://www.evomag.ro"));
//        access.bypassCampaign();
            categoryRandomizer.catRand(actions, driver);
            for(int i=0; i<3; i++) {
                cartRandomizer.cartRand();
            }
            driver.navigate().to("https://www.evomag.ro/cart/sendOrder");
            wishlist.wish(driver, "WishlistTest2", "This list will be private");
            wishlist.makePrivate(driver);
            driver.navigate().to("https://www.evomag.ro/wishlist/index");
            wishlist.wishAssert(driver);
            driver.navigate().to("https://www.evomag.ro");
            logout.logoutAccount(actions);
        } catch (Exception e) {
            System.out.println("The test encountered the following error " + e.getMessage());
            e.printStackTrace();
        }
    }
}
