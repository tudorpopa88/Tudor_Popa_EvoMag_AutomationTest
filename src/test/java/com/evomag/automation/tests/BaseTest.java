package com.evomag.automation.tests;

import com.evomag.automation.utils.WebBrowser;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {
    WebDriver driver = null;
    WebBrowser.Browsers browserType;

    /**This method runs before any test method belonging to the classes configured to run
     * and starts the driver used (Firefox, Chrome, IE or HTMLUNIT)
     * @see WebBrowser for more details
     */
    @BeforeTest
    public void startDriver() {
        try {
            driver = WebBrowser.getDriver(WebBrowser.Browsers.CHROME);
        }
        catch(Exception ex) {
            System.out.println("Browser specified is not on the browser supported list. Try Firefox, Chrome, IE or HTMLUNIT");
        }
    }

    /**This method runs after all the test methods belonging
     * to the classes configured to run and close the driver
     *
     */
    @AfterTest
    public void closeDriver() {
        try {
            driver.close();
        }
        catch (Exception ex) {
            System.out.println("No driver was to close");
        }
    }
}
