package com.evomag.automation.tests;

import com.evomag.automation.pages.RegisterPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class RegisterTest extends BaseTest {

    /**This method is used to test SQL integration with the DataProvider
     *
     * @return The annotated method must return an Object[][] where each Object[] can be assigned
     * the parameter list of the test method.
     * @throws SQLException
     */
    @DataProvider(name = "RegisterDataProvider")
    public Iterator<Object[]> registerDataProvider() throws SQLException {
        String host = "localhost";
        String port = "3306";
        Connection conn = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/evomag_aut", "root", "delphi");
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.getResultSet();
        Collection<Object[]> dp = new ArrayList<Object[]>();
        for(int recordNumber = 1; recordNumber < 9; recordNumber++) {
            resultSet = statement.executeQuery("SELECT * FROM register WHERE id = '" + recordNumber + "'");
            while(resultSet.next()) {
                dp.add(new String[]{resultSet.getString("name"), resultSet.getString("email"), resultSet.getString("password")});
            }
        }
        resultSet.close();
        conn.close();
        return dp.iterator();

//        name | email | password (from 'register' table within 'evomag_aut' database)
//        ----------------------------------------------------------------------------
//        {Marian Birtoiu}, {marian_birtoiu_123@gmail.com}, {marian123}
//        {Marian Birtoiu}, {}, {marian123}
//        {}, {marian_birtoiu_123@gmail.com}, {}
//        {}, {}, {}
//        {d*kjD29(c@cd9Dx$jk}, {o$cW^3lC32kZ0#X!;8}, {9v$dX8O1x8Br%d9#k)}
//        {jf#j>9K2d^2}, {1234@yahoo.com}, {}
//        {k&c@8z9G8n$inp}, {*#&@()@gmail.hotmail.com}, {d&3F2Be!pmOp}
//        {marian@gmail.com}}, {0710920818}, {    Marian    }
//        {0720231090}, {Kvj&3(DCm<dO9vD0}, {marmarmarmarmarmar}
    }

    /**This method is used to test the user's credentials given by the DataProvider to register on site
     *
     * @param fullName is the new name
     * @param email is the new email address
     * @param password is the new password
     * @see RegisterPage for more details
     */
    @Test(dataProvider = "RegisterDataProvider")
    public void RegisterTest(String fullName, String email, String password) {
        try {
            driver.manage().window().maximize();
            driver.get("https://www.evomag.ro/client/auth");
            RegisterPage register = PageFactory.initElements(driver, RegisterPage.class);
            register.register(driver, fullName, email, password);
        }
        catch (Exception e) {
            System.out.println("The test encountered the following error " + e.getMessage());
            e.printStackTrace();
        }
    }
}
