package com.evomag.automation.tests;

import com.evomag.automation.pages.CookiesPage;
import com.evomag.automation.pages.SearchComplexPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class SearchComplexTest extends BaseTest{

    /**This method is used to pass different test scenarios to the test method
     *
     * @return The annotated method must return an Object[][] where each Object[] can be assigned
     * the parameter list of the test method.
     */
    @DataProvider(name = "SearchDataProvider")
    public Iterator<Object[]> searchDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[] {"laptop dell", "valid"});
        dp.add(new String[] {"sownzpdfkwxldo", "invalid"});
        dp.add(new String[] {"381053871019", "invalid"});
        dp.add(new String[] {"^!(<#)$@($?/", "invalid"});
        dp.add(new String[] {"so&53)bw*2xwlp10", "invalid"});
        dp.add(new String[] {"djslwaqkdmszcjrdkglvmvjkd583922403822012394028420248320213028324"});
        dp.add(new String[] {"", "invalid"});
        return dp.iterator();
    }

    /**This method is used to test the search feature along with DataProvider
     *
     * @param searchData is the search query
     * @param searchType is the validity type
     * @see SearchComplexPage for more details
     * @see CookiesPage for more details
     */
    @Test(dataProvider = "SearchDataProvider")
    public void searchComplexTest(String searchData, String searchType) {
        try {
            driver.manage().window().maximize();
            driver.get("https://www.evomag.ro");
//            BypassCampaignPage access = PageFactory.initElements(driver, BypassCampaignPage.class);
//            access.bypassCampaign();
            SearchComplexPage searchComplex = PageFactory.initElements(driver, SearchComplexPage.class);
            searchComplex.searchComplex(driver, searchData, searchType);
            CookiesPage cookies = PageFactory.initElements(driver, CookiesPage.class);
            cookies.cookExists(driver);
            cookies.cookDelete(driver);
        } catch (Exception e) {
            System.out.println("The test encountered the following error " + e.getMessage());
            e.printStackTrace();
        }
    }
}
