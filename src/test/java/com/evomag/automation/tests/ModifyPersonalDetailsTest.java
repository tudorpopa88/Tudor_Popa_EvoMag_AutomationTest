package com.evomag.automation.tests;

import com.evomag.automation.pages.LoginTestAccountPage;
import com.evomag.automation.pages.LogoutPage;
import com.evomag.automation.pages.ModifyPersonalDetailsPage;
import com.evomag.automation.pages.PersonalDetailsPage;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class ModifyPersonalDetailsTest extends BaseTest{

    /**This method to pass different test scenarios to the test method
     *
     * @return The annotated method must return an Object[][] where each Object[] can be assigned
     * the parameter list of the test method.
     */
    @DataProvider(name = "ModifyDetailsDataProvider")
    public Iterator<Object[]> modifyDetailsDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[] {"", "", "", "Campul Nume nu poate fi gol.", "Campul Adresa e-Mail nu poate fi gol.", "Campul Telefon 1 nu poate fi gol.", "first"});
        dp.add(new String[] {"Ion Popescu", "", "", "", "Campul Adresa e-Mail nu poate fi gol.", "Campul Telefon 1 nu poate fi gol.", "middle"});
        dp.add(new String[] {"", "ionpopescu30@gmail.com", "", "Campul Nume nu poate fi gol.", "", "Campul Telefon 1 nu poate fi gol.", "middle"});
        dp.add(new String[] {"", "", "0002223456", "Campul Nume nu poate fi gol.", "Campul Adresa e-Mail nu poate fi gol.", "", "middle"});
        dp.add(new String[] {"", "ionpopescu30@gmail.com", "0002223456", "Campul Nume nu poate fi gol.", "", "", "middle"});
        dp.add(new String[] {"Ion Popescu", "", "0002223456", "", "Campul Adresa e-Mail nu poate fi gol.", "", "middle"});
        dp.add(new String[] {"^9je20L9E@cz$P`", "0024", "@mail.com.ro", "Campul Adresa e-Mail nu poate fi gol.", "Campul Telefon 1 nu poate fi gol.", "middle"});
        dp.add(new String[] {"", "@jfd(FL2$>%wo", "011132004100291110900", "Campul Nume nu poate fi gol.", "Campul Adresa e-Mail nu poate fi gol.", "", "middle"});
        dp.add(new String[] {"Ion Popescu", "ionpopescu30@gmail.com", "", "", "", "Campul Telefon 1 nu poate fi gol.", "last"});
//        dp.add(new String[] {"Ion Popescu", "ionpopescu30@gmail.com", "0002223456", "", "", ""});
        return dp.iterator();
    }

    /**This method is used to test the personal details page along with DataProvider
     *
     * The steps are:
     * 1. Login to test account
     * 2. Open personal details page
     * 3. Modify the user's personal info
     * 4. Logout
     *
     * @param fullName is the new first and last name
     * @param email is the new email address
     * @param phone is the new phone
     * @param errorName is the new full name error message
     * @param errorEmail is the new email error message
     * @param errorPhone is the new phone error message
     * @param runLoginFlag is the script's condition to either call or not the login and personal details page
     * @see LoginTestAccountPage for more details
     * @see PersonalDetailsPage for more details
     * @see ModifyPersonalDetailsPage for more details
     * @see LogoutPage for more details
     */
    @Test(dataProvider = "ModifyDetailsDataProvider")
    public void modifyPersonalDetailsTest(String fullName, String email, String phone, String errorName, String errorEmail, String errorPhone, String runLoginFlag) {
        try {
            driver.manage().window().maximize();
            driver.get("https://www.evomag.ro/client/auth");
            LoginTestAccountPage loginTestAccount = PageFactory.initElements(driver, LoginTestAccountPage.class);
            PersonalDetailsPage personalDetails = PageFactory.initElements(driver, PersonalDetailsPage.class);
            ModifyPersonalDetailsPage modifyPersonalDetails = PageFactory.initElements(driver, ModifyPersonalDetailsPage.class);
            LogoutPage logout = PageFactory.initElements(driver, LogoutPage.class);
            Actions actions = new Actions(driver);
            if(runLoginFlag.equals("first")) {
                loginTestAccount.loginTestAcc("marius.dragomir.50@gmail.com", "marius5050");
//                BypassCampaignPage access = PageFactory.initElements(driver, BypassCampaignPage.class);
//                access.bypassCampaign();
                personalDetails.persDetails(actions);
            }
            modifyPersonalDetails.modifyPersonalDetails(driver, fullName, email, phone, errorName, errorEmail, errorPhone);
            if(runLoginFlag.equals("last")) {
                driver.navigate().to("https://www.evomag.ro");
                logout.logoutAccount(actions);
            }
        } catch (Exception e) {
            System.out.println("The test encountered the following error " + e.getMessage());
            e.printStackTrace();
        }
    }
}
