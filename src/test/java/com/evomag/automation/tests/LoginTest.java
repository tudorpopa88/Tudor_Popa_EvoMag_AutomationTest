package com.evomag.automation.tests;

import com.evomag.automation.pages.AccountModelPage;
import com.evomag.automation.pages.LoginModelPage;
import com.evomag.automation.pages.LoginPage;
import com.evomag.automation.reader.CSVReader;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LoginTest extends BaseTest {

    /**This method is used to test login connection to the CSV file
     *
     * @return The annotated method must return an Object[][] where each Object[] can be assigned
     * the parameter list of the test method.
     * @throws Exception
     */
    @DataProvider(name = "CSVDataProvider")
    public Object[][] csvDataProviderCollection() throws Exception {
        ClassLoader cl = getClass().getClassLoader();
        //File csvFile = new File(cl.getResource("src\\main\\resources\\datasource\\loginData.csv").getFile());
        List<File> files = getListOfFiles("src\\main\\resources\\dataSource", "csv");
        File csvFile = files.get(0);
        List<String[]> csvData = CSVReader.readCSVFile(csvFile);
        /*List<String[]> csvData = new ArrayList<String[]>();
        CSVReader csvReader = new CSVReader(new FileReader(csvFile));
        String[] record = null;
        while ((record = csvReader.readNext())!=null) {
            csvData.add(record);
            System.out.println(record);
        }*/

        Object[][] dp = new Object[csvData.size()][1];
        for (int i = 0; i < dp.length; i++) {
            LoginModelPage loginModel = new LoginModelPage();
            try{
                AccountModelPage account = new AccountModelPage();
                account.setUsername(csvData.get(i)[0]);
                account.setPassword(csvData.get(i)[1]);

                loginModel.setAccountModel(account);
//                loginModel.setUserError(csvData.get(i)[2]);
//                loginModel.setPasswordError(csvData.get(i)[3]);
//                loginModel.setGeneralError(csvData.get(i)[4]);
            }
            catch (Exception e){
                System.out.println("This error is: " + e.getMessage());
            }

            dp[i][0] = loginModel;
        }
        return dp;

    }

    /**This method is used to test login connection
     *
     * @param loginModelPage is used to pass the user's credentials
     * @see LoginPage for more details
     */
    @Test(dataProvider = "CSVDataProvider")
    public void loginTestCsv(LoginModelPage loginModelPage) {
        String loginError = "Nu va puteti autentifica! Adresa de email introdusa este invalida!";
        try{
            driver.manage().window().maximize();
            driver.get("https://www.evomag.ro/client/auth");
            LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
            loginPage.login(driver,loginModelPage.getAccountModel().getUsername(),loginModelPage.getAccountModel().getPassword());
//            Assert.assertEquals(loginError, "Nu va puteti autentifica! Adresa de email introdusa este invalida!");
        }
        catch (Exception e){
            System.out.println("The test encountered the following error " + e.getMessage());
            e.printStackTrace();
        }

    }


    private List<File> getListOfFiles(String path, String ext) {
        List<File> textFiles = new ArrayList<File>();
        File dir = new File(path);
        for (File file : dir.listFiles()) {
            if (file.getName().endsWith("." + ext)) {
                textFiles.add(file);
            }
        }
        return textFiles;
    }


//    @DataProvider(name = "LoginDataProvider")
//    public Iterator<Object[]> loginDataProvider() {
//        Collection<Object[]> dp = new ArrayList<Object[]>();
//        dp.add(new String[] {"", ""});
//        dp.add(new String[] {"george_badea@gmail.com", ""});
//        dp.add(new String[] {"", "geoboss123"});
//        dp.add(new String[] {"george_badea@gmail.com", "geoboss123"});
//        dp.add(new String[] {"@xxx", "fpx"});
//        dp.add(new String[] {"Kw*01cwnD8^!c62)jd)_vqDL&89#", "m#9)wN1I*`Dv*2p+dv(%82%9="});
//        return dp.iterator();
//    }
//
//    @Test(dataProvider = "LoginDataProvider")
//    public void LoginTest(String emailPhone, String password) {
//        try {
//            driver.manage().window().maximize();
//            driver.get("https://www.evomag.ro/client/auth");
//            Actions actions = new Actions(driver);
//            LoginPage lp = PageFactory.initElements(driver, LoginPage.class);
//            lp.login(driver, emailPhone, password);
//        }
//        catch (Exception e) {
//            System.out.println("The test encountered the following error " + e.getMessage());
//            e.printStackTrace();
//        }
//    }

}
