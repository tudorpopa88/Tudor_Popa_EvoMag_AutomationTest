package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

/**
 * Created by tudor on 10/29/2017.
 */
public class WishlistPage {

    @FindBy(how = How.XPATH, using = ".//*[@class='buton_wish_tab sterge_tab_cos']")
    private WebElement emptyCart;

    @FindBy(how = How.XPATH, using = ".//*[@class='buton_wish_tab'][2]")
    private WebElement createWishlist;

    @FindBy(how = How.ID, using = "Wishlist_Name")
    private WebElement wishlistName;

    @FindBy(how = How.ID, using = "Wishlist_Comment")
    private WebElement wishlistComment;

    @FindBy(how = How.ID, using = "Wishlist_VisibleForAll")
    private WebElement isPublicWishlist;

    @FindBy(how = How.NAME, using = "yt0")
    private WebElement saveWishlist;

    /**This method is used to create a wishlist and fill in some details
     *
     * @param driver is the WebDriver
     * @param name is the wishlist name
     * @param comment is the wishlist comment
     */
    public void wish(WebDriver driver, String name, String comment) {
        ThreadSleeper.mySleeper(1000L);
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollTo(0, 800)");

        createWishlist.click();
        ThreadSleeper.mySleeper(1000L);
        wishlistName.sendKeys(name);
        wishlistComment.sendKeys(comment);
    }

    /**This method is used to make the wishlist public
     *
     * @param driver is the WebDriver
     */
    public void makePublic(WebDriver driver) {
        if(isPublicWishlist.isSelected() == false) {
            isPublicWishlist.click();
        }
        saveWishlist.click();
        driver.get("https://www.evomag.ro/cart/sendOrder");
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollTo(0, 800)");
        ThreadSleeper.mySleeper(1000L);
        emptyCart.click();
    }

    /**This method is used to make the wishlist private
     *
     * @param driver is the WebDriver
     */
    public void makePrivate(WebDriver driver) {
        if(isPublicWishlist.isSelected() == true) {
            isPublicWishlist.click();
        }
        saveWishlist.click();
        driver.get("https://www.evomag.ro/cart/sendOrder");
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollTo(0, 800)");
        ThreadSleeper.mySleeper(1000L);
        emptyCart.click();
    }

    /**This method asserts the public and private wishlists with their displayed visibility level
     *
     * @param driver is the WebDriver
     */
    public void wishAssert(WebDriver driver) {
        ThreadSleeper.mySleeper(1000L);
        Assert.assertEquals(driver.findElement(By.xpath(".//*[@id='yw0']/table/tbody/tr[1]/td[3]")).getText(), "Nu");
        Assert.assertEquals(driver.findElement(By.xpath(".//*[@id='yw0']/table/tbody/tr[2]/td[3]")).getText(), "Da");

    }
}
