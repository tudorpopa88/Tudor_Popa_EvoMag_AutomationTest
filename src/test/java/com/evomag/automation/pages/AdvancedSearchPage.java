package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;

public class AdvancedSearchPage {

    @FindBy(how = How.XPATH, using = ".//*[@class='taburi_top']/ul/li[8]/a")
    private WebElement advSearch;

    @FindBy(how = How.ID, using = "Article_minPrice")
    private WebElement minPrice;

    @FindBy(how = How.ID, using = "Article_maxPrice")
    private WebElement maxPrice;

    @FindBy(how = How.ID, using = "sel-all-stock")
    private WebElement selAllStock;

    @FindBy(how = How.ID, using = "sel-all-manufacturer")
    private WebElement selAllManuf;

    @FindBy(how = How.ID, using = "sel-all-warranty")
    private List<WebElement> selAllWarranty;

    @FindBy(how = How.XPATH, using = ".//*[@class='set_proprietati'][2]/tbody/tr[3]/td/input")
    private List<WebElement> allStockTypes;

    @FindBy(how = How.XPATH, using = ".//*[@class='set_proprietati'][3]/tbody/tr[1]/following-sibling::tr/td/input")
    private List<WebElement> allManufTypes;

    @FindBy(how = How.XPATH, using = ".//*[@class='set_proprietati'][4]/tbody/tr[1]/following-sibling::tr/td/input")
    private List<WebElement> allWarrantyTypes;

    @FindBy(how = How.XPATH, using = ".//*[@value='Cauta produse conform cu criteriile de mai jos']")
    private WebElement selSearchCriteria;

    @FindBy(how = How.XPATH, using = ".//*[@class='product_grid']")
    private WebElement generalErrorSearch;

    @FindBy(how = How.XPATH, using = ".//*[@class='fa fa-times close-footer']")
    private WebElement footerClose;

    /**This method is used to check the validity of the price range,
     * assert the connection between the select all and the corresponding buttons
     * for the state of stock, manufacturers and warranty sub-sections
     * and assert the general error message for the invalid criteria
     *
     * @param driver is the WebDriver
     * @param minPrice is the minimum price
     * @param maxPrice is the maximum price
     */
    public void advSearch(WebDriver driver, String minPrice, String maxPrice) {
        ThreadSleeper.mySleeper(2000L);
        if(advSearch.isEnabled()) {
            advSearch.click();
        }
        ThreadSleeper.mySleeper(2000L);
        this.minPrice.clear();
        this.minPrice.sendKeys(minPrice);
        this.maxPrice.clear();
        this.maxPrice.sendKeys(maxPrice);

        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollTo(0,150)");
        selAllStock.click();
        int stockCount = allStockTypes.size();
        for(int i=0; i<stockCount; i++) {
            Assert.assertEquals(selAllStock.isSelected(), allStockTypes.get(stockCount - 1).isSelected());
        }
        jse.executeScript("window.scrollTo(0, 250)");

        selAllManuf.click();
        int manufCount = allManufTypes.size();

        for(int i=0; i<manufCount; i++) {
            Assert.assertEquals(selAllManuf.isSelected(), allManufTypes.get(manufCount - 1).isSelected());
        }

        footerClose.click();
        if(selAllWarranty.size() > 0) {
            selAllWarranty.get(0).click();
            int warrantyCount = allWarrantyTypes.size();
            for(int i=0; i<warrantyCount; i++) {
                Assert.assertEquals(selAllWarranty.get(0).isSelected(), allWarrantyTypes.get(warrantyCount - 1).isSelected());
            }
        }

        jse.executeScript("window.scrollTo(0, 150)");
        ThreadSleeper.mySleeper(2000L);
        selSearchCriteria.click();

    }

    public void searchErrorAssert() {
        Assert.assertEquals("Nu au fost găsite produse corespunzătoare criteriilor alese.", generalErrorSearch.getText());
    }

}
