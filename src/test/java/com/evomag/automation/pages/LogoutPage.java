package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LogoutPage {

    @FindBy(how = How.XPATH, using = ".//*[@id='personal_header']/div[2]/div[1]")
    private WebElement accountMenu;

    @FindBy(how = How.XPATH, using = ".//*[@id='personal_header']/div[2]/div[2]/div[13]/a")
    private WebElement logoutButton;

    /**This method is used to logout
     *
     * @param actions is the Actions interaction
     */
    public void logoutAccount(Actions actions) {
        ThreadSleeper.mySleeper(1000L);
        actions.moveToElement(accountMenu).build().perform();
        logoutButton.click();
    }
}
