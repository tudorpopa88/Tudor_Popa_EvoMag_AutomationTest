package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;
import java.util.Random;

/**
 * Created by tudor on 10/29/2017.
 */
public class CategoryRandomizerPage {
    @FindBy(how = How.XPATH, using = ".//*[@id='menu_catex']/div[1]")
    private WebElement categoryButton;

    @FindBy(how = How.XPATH, using = ".//*[@id='menu_catex']/div[2]/div/ul/li")
    private List<WebElement> categoryList;

    @FindBy(how = How.XPATH, using = ".//*[@id='menu_catex']/div[2]/div/ul/li/a")
    private List<WebElement> categoryListNames;

    @FindBy(how = How.XPATH, using = ".//*[@class='meniu_produse_list']/h1")
    private WebElement categoryTitle;

    /**This method is used to select a random category page
     * and avoid the categories without add to cart buttons and advanced search option
     *
     * @param actions is the Actions interaction
     * @param driver is the WebDriver
     */
    public void catRand(Actions actions, WebDriver driver) {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollTo(0, 200)");
        ThreadSleeper.mySleeper(5000L);
        int count = categoryList.size();
        Random rand = new Random();
        int randCatID = rand.nextInt(count) + 1;
        System.out.println("The selected position is " + randCatID + " from the category " + categoryListNames.get(randCatID - 1).getText());
        if(randCatID == 14 || randCatID == 15 || randCatID == 16 || randCatID == 17 || randCatID == 18 || randCatID == 21) {
            randCatID = rand.nextInt(count) + 1;
        }
        if(categoryList.get(randCatID).isDisplayed() == false) {
            actions.moveToElement(categoryButton).build().perform();
        }
        ThreadSleeper.mySleeper(5000L);
        actions.moveToElement(categoryList.get(randCatID - 1)).build().perform();
        String subCatID = categoryList.get(randCatID - 1).getAttribute("data-submenu-id");
        System.out.println("The selected subcategory is " + categoryList.get(randCatID - 1).findElement(By.xpath(".//*[@id='" + subCatID + "']/div[*]/ul/li[*]/a")).getText());
        ThreadSleeper.mySleeper(5000L);
        categoryList.get(randCatID - 1).findElement(By.xpath(".//*[@id='" + subCatID + "']/div[*]/ul/li[*]/a")).click();
    }
}
