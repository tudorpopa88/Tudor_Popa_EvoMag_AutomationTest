package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;
import java.util.Random;

/**
 * Created by tudor on 11/5/2017.
 */
public class BrandValidationPage {

    @FindBy(how = How.XPATH, using = ".//*[@class='prod manufacturer_list']/li[*]/a")
    private List<WebElement> prodManufacturersGeneral;

    @FindBy(how = How.XPATH, using = ".//*[@class='prod manufacturer_list']/li[*]/a/span")
    private List<WebElement> prodManufacturersSize;

    @FindBy(how = How.XPATH, using = ".//*[@class='product_grid']/div[*]/div/div[3]/a")
    private List<WebElement> prodDisplayedNames;

    @FindBy(how = How.XPATH, using = ".//*[@id='yw0']/li/a")
    private List<WebElement> pageNumbers;

    @FindBy(how = How.CLASS_NAME, using = "nice_product_container")
    private List<WebElement> productsOnPage;

    @FindBy(how = How.XPATH, using = ".//*[@class='a_sel']/span")
    private WebElement selectedProdSize;

    /**This method is used to select a random brand and assert its name with the name displayed on page
     * and assert the number of products from category pane with the number of products present on the page
     *
     * @param driver is the WebDriver
     */
    public void brandNameVal(WebDriver driver) {
        ThreadSleeper.mySleeper(1000L);
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollTo(0, 500)");
        int manufList = prodManufacturersGeneral.size();
        Random rand = new Random();
        int manufCount = rand.nextInt(manufList) + 1;
        System.out.println("The selected manufacturer has the number " + manufCount);
        String selectedManufName = prodManufacturersGeneral.get(manufCount - 1).getText();
        String selectedProdManufSize = prodManufacturersSize.get(manufCount - 1).getText();
        System.out.println("The selected manufacturer is " + selectedManufName);
        prodManufacturersGeneral.get(manufCount - 1).click();

        ThreadSleeper.mySleeper(1000L);
        int nameList = prodDisplayedNames.size();
        System.out.println("The selected product is " + prodDisplayedNames.get(nameList - 1).getText());
        Assert.assertTrue(prodDisplayedNames.get(nameList - 1).getText().contains(selectedManufName.substring(0, selectedManufName.length() - selectedProdManufSize.length())));
    }

    public void brandNumberVal() {
        ThreadSleeper.mySleeper(1000L);
        int pagesCount = pageNumbers.size();
        ThreadSleeper.mySleeper(1000L);
        int totalProdsOnScreen;
        if (pagesCount > 0) {
            pageNumbers.get(pagesCount - 1).click();
            int finalpagesCount = pageNumbers.size();
            int prodsOnPageCount = productsOnPage.size();
            int pageNumberInt = Integer.parseInt(pageNumbers.get(finalpagesCount - 3).getText());
            totalProdsOnScreen = (pageNumberInt - 1) * 16 + prodsOnPageCount;
            System.out.println("The number of products displayed on screen is " + totalProdsOnScreen);
        } else {
            totalProdsOnScreen = productsOnPage.size();
            System.out.println("The number of products displayed on screen is " + totalProdsOnScreen);
        }
        String size1 = selectedProdSize.getText().replace("(", "");
        String size2 = size1.replace(")", "");
        int totalProdsOnManufList = Integer.parseInt(size2);
        System.out.println("The number of products displayed on manufacturers list is " + totalProdsOnManufList);
        Assert.assertEquals(totalProdsOnManufList, totalProdsOnScreen);
    }

}
