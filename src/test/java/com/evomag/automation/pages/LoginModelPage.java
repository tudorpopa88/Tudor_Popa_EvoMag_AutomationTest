package com.evomag.automation.pages;

/**
 * Created by tudor on 11/2/2017.
 */
public class LoginModelPage {
    private AccountModelPage accountModel;
    private String userError;
    private String passwordError;
    private String generalError;

    public String getUserError() {
        return userError;
    }

    public void setUserError(String userError) {
        this.userError = userError;
    }

    public String getPasswordError() {
        return userError;
    }

    public void setPasswordError(String passwordError) {
        this.passwordError = passwordError;
    }

    public String getGeneralError() {
        return generalError;
    }

    public void setGeneralError(String generalError) {
        this.generalError = generalError;
    }

    public AccountModelPage getAccountModel() {
        return accountModel;
    }

    public void setAccountModel(AccountModelPage accountModel) {
        this.accountModel = accountModel;
    }
}
