package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

/**
 * Created by tudor on 10/29/2017.
 */
public class SearchComplexPage {
    @FindBy(how = How.XPATH, using = ".//*[@id='searchString'][2]")
    private WebElement searchField;

    @FindBy(how = How.XPATH, using = ".//*[@id='top_search']/div/div[1]/input")
    private WebElement searchButton;

    @FindBy(how = How.XPATH, using = ".//*[@class='meniu_produse_list searchnode']/h1")
    private WebElement searchMessageFound;

    @FindBy(how = How.XPATH, using = ".//*[@class='produse_liste_filter']/h3/div/div")
    private WebElement searchMessageNotFound;

    /**This method is used to test different search queries, valid or invalid
     * and assert the error message
     *
     * @param driver is the WebDriver
     * @param sData is the search query
     * @param searchType is the search type valid or invalid
     */
    public void searchComplex(WebDriver driver, String sData, String searchType) {
        ThreadSleeper.mySleeper(2000L);
        searchField.clear();
        searchField.sendKeys(sData);
        searchField.sendKeys(Keys.ENTER);
        if(searchType.equals("valid")) {
            String searchMessageQuery = searchMessageFound.getText().replace("Rezultate cautare pentru ", "").replace("'", "");
            Assert.assertEquals(sData, searchMessageQuery);
        }
        if(searchType.equals("invalid")) {
            Assert.assertEquals("CRITERIILE DE FILTRARE SELECTATE DE DUMNEAVOASTRA NU AU RETURNAT NICI UN REZULTAT!", searchMessageNotFound.getText());
        }
        ThreadSleeper.mySleeper(3000L);
        driver.navigate().back();
    }
}
