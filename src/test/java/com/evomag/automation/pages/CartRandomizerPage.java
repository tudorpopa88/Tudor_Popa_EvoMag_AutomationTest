package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;
import java.util.Random;

/**
 * Created by tudor on 10/29/2017.
 */
public class CartRandomizerPage {
    @FindBy(how = How.XPATH, using = ".//input[@value='ADAUGA IN COS']")
    private List<WebElement> randomCart;

    @FindBy(how = How.XPATH, using = ".//*[@id='CrossSaleModal']/img")
    private WebElement closeButton;

    /**This method is used to select a random product from the currently displayed page
     * and add it to cart
     *
     */
    public void cartRand() {
        ThreadSleeper.mySleeper(7000L);
        Random rand = new Random();
        int counter = rand.nextInt(randomCart.size()) + 1;
        System.out.println("The selected cart has number " + counter);
        randomCart.get(counter - 1).submit();
        ThreadSleeper.mySleeper(7000L);
        closeButton.click();
    }
}
