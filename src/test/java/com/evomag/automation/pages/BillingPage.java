package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

/**
 * Created by tudor on 10/29/2017.
 */
public class BillingPage {

    @FindBy(how = How.XPATH, using = ".//*[@id='personal_header']/div[1]/div/a/div")
    private WebElement shoppingBasket;

    @FindBy(how = How.ID, using = "Client_Name")
    private WebElement fullNameField;

    @FindBy(how = How.ID, using = "Client_CNP")
    private WebElement cnpField;

    @FindBy(how = How.ID, using = "Client_Phone")
    private WebElement phoneField;

    @FindBy(how = How.ID, using = "PartnerAddress_county")
    private WebElement county;

    @FindBy(how = How.ID, using = "PartnerAddress_CityId")
    private WebElement cityID;

    @FindBy(how = How.ID, using = "PartnerAddress_Address")
    private WebElement address;

    @FindBy(how = How.ID, using = "PartnerAddress_PostalCode")
    private WebElement postalCode;

//    @FindBy(how = How.ID, using = "DocumentProperty_minDeliveryHours")
//    private WebElement startHour;
//
//    @FindBy(how = How.ID, using = "DocumentProperty_maxDeliveryHours")
//    private WebElement endHour;

    @FindBy(how = How.ID, using = "DocumentProperty_Phonecall")
    private WebElement phoneCall;

    @FindBy(how = How.ID, using = "SalesOrder_Comment")
    private WebElement saleComment;

    @FindBy(how = How.ID, using = "conditii")
    private WebElement conditiiConf;

    @FindBy(how = How.ID, using = "termeni")
    private WebElement termeniUtil;

    @FindBy(how = How.ID, using = "confirmare")
    private WebElement confirmTel;

    @FindBy(how = How.ID, using = "DocumentProperty_SMS")
    private WebElement smsComanda;

    /**This method is used to fill the billing page with relevant data
     * and assert the selections with their real values displayed on page
     *
     * @param driver is the WebDriver
     */
    public void billing(WebDriver driver) {
        ThreadSleeper.mySleeper(3000L);
        shoppingBasket.click();
        ThreadSleeper.mySleeper(3000L);
        fullNameField.clear();
        fullNameField.sendKeys("Marius Dragomir");
        cnpField.clear();
        cnpField.sendKeys("1820520418209");
        phoneField.clear();
        phoneField.sendKeys("0745208705");
        Select s1 = new Select(county);
        s1.selectByValue("Braila");
        System.out.println("The selected county is " + s1.getFirstSelectedOption().getText());
        Assert.assertEquals("Braila", s1.getFirstSelectedOption().getText());

        ThreadSleeper.mySleeper(1000L);
        Select s2 = new Select(cityID);
        s2.selectByVisibleText("Maxineni");
        System.out.println("The selected city is " + s2.getFirstSelectedOption().getText());
        Assert.assertEquals("Maxineni", s2.getFirstSelectedOption().getText());

        address.sendKeys("Str. Soarelui, nr. 45, bl. P2, sc. 1, et. 2, ap. 25");
        postalCode.sendKeys("32810");

//        JavascriptExecutor jse = (JavascriptExecutor)driver;
//        jse.executeScript("window.scrollTo(0, 300)");

//        ThreadSleeper.mySleeper(1500L);

//        int count1 = driver.findElements(By.xpath(".//*[@class='payment_method_cell']/input")).size();
//        for(int i = 0; i<count1; i++) {
//            String text = driver.findElements(By.xpath(".//*[@class='payment_method_cell']/label")).get(i).getText();
//            if(text.equals("Bitcoin")) {
//                driver.findElements(By.xpath(".//*[@class='payment_method_cell']/input")).get(i).click();
//                break;
//            }
//        }

        ThreadSleeper.mySleeper(1000L);
        int count2 = driver.findElements(By.xpath(".//*[@class='shipping_method_cell']/input")).size();
        for(int i=0; i<count2; i++) {
            String text = driver.findElements(By.xpath(".//*[@class='shipping_method_cell']/label")).get(i).getText();
            if(text.equals("Livrare prin curier")) {
                driver.findElements(By.xpath(".//*[@class='shipping_method_cell']/input")).get(i).click();
                Assert.assertEquals("Livrare prin curier", driver.findElements(By.xpath(".//*[@class='shipping_method_cell']/label")).get(i).getText());
            }
        }
//        Select s3 = new Select(startHour);
//        s3.selectByValue("14");
//        Select s4 = new Select(endHour);
//        s4.selectByValue("17");
        phoneCall.click();
        saleComment.sendKeys("Miercuri sunt plecat din localitate.");

        if(conditiiConf.isSelected() == false) {
            conditiiConf.click();
        }
        if(termeniUtil.isSelected() == false) {
            termeniUtil.click();
        }
        if(confirmTel.isSelected() == true) {
            confirmTel.click();
        }
        if(smsComanda.isSelected() == false) {
            smsComanda.click();
        }

        ThreadSleeper.mySleeper(2000L);
    }
}
