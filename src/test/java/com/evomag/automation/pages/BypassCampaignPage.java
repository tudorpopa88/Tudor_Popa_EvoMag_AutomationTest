package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by tudor on 11/2/2017.
 */
public class BypassCampaignPage {

    @FindBy(how = How.XPATH, using = ".//*[@id='header']/div[2]/div/div/a[1]")
    private WebElement returnToSite;

    /**This method is used to bypass the seasonal campaign page which is ofter introduced
     * at the first launch of the main page
     *
     */
    public void bypassCampaign() {
        ThreadSleeper.mySleeper(1000L);
        returnToSite.click();
    }
}
