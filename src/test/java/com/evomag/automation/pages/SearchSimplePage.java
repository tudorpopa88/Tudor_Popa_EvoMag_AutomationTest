package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SearchSimplePage {
    @FindBy(how = How.XPATH, using = ".//*[@id='searchString'][2]")
    private WebElement searchField;

    /**Is the basic search method used for only valid entries
     *
     * @param searchQuery is the search query
     */
    public void search(String searchQuery) {
        ThreadSleeper.mySleeper(2000L);
        searchField.sendKeys(searchQuery);
        searchField.sendKeys(Keys.ENTER);
    }
}
