package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;
import java.util.Random;

public class ModifyPersonalDetailsPage {

    @FindBy(how = How.ID, using = "Partner_Name")
    private WebElement fullNameField;

    @FindBy(how = How.ID, using = "Partner_Email")
    private WebElement emailField;

    @FindBy(how = How.ID, using = "Partner_Phone")
    private WebElement phoneField;

    @FindBy(how = How.ID, using = "Partner_Name_em_")
    private WebElement errorFullName;

    @FindBy(how = How.ID, using = "Partner_Email_em_")
    private WebElement errorEmail;

    @FindBy(how = How.ID, using = "Partner_Phone_em_")
    private WebElement errorPhone;

    @FindBy(how = How.ID, using = "save")
    private WebElement saveChanges;

    @FindBy(how = How.XPATH, using = ".//*[@id='Partner_GenderId']/input")
    private List<WebElement> genders;

    @FindBy(how = How.ID, using = "ziua")
    private WebElement day;

    @FindBy(how = How.ID, using = "luna")
    private WebElement month;

    @FindBy(how = How.ID, using = "anul")
    private WebElement year;

    @FindBy(how = How.ID, using = "newsletter")
    private WebElement newsletter;

    @FindBy(how = How.ID, using = "changePassword")
    private WebElement changePassword;

    @FindBy(how = How.ID, using = "User_Password")
    private WebElement userPassword;

    @FindBy(how = How.ID, using = "User_passwordCompare")
    private WebElement userPasswordConfirm;

    /**This method is used to modify the user's personal details
     * and assert the error message for the mandatory fields like full name, email and phone
     * It also randomly selects some buttons and drop down menus for gender and birthday
     *
     * @param driver is the WebDriver
     * @param fullName is the first and last name
     * @param email is the email address
     * @param phone is the phone
     * @param errorFullName is the full name error
     * @param errorEmail is the email address error
     * @param errorPhone is the phone error
     */
    public void modifyPersonalDetails(WebDriver driver, String fullName, String email, String phone, String errorFullName, String errorEmail, String errorPhone) {

        ThreadSleeper.mySleeper(2000L);
        int genderCount = genders.size();
        Random rand = new Random();
        int randGender = rand.nextInt(genderCount) + 1;
        genders.get(randGender - 1).click();

        fullNameField.clear();
        fullNameField.sendKeys(fullName);
        emailField.clear();
        emailField.sendKeys(email);
        phoneField.clear();
        phoneField.sendKeys(phone);

        Select daySelector = new Select(day);
        int dayIndex = driver.findElements(By.xpath(".//*[@id='ziua']/option")).size();
        int randDay = rand.nextInt(dayIndex) + 1;
        String selectedDay = driver.findElements(By.xpath(".//*[@id='ziua']/option")).get(randDay).getText();
        daySelector.selectByVisibleText(selectedDay);

        Select monthSelector = new Select(month);
        int monthIndex = driver.findElements(By.xpath(".//*[@id='luna']/option")).size();
        int randMonth = rand.nextInt(monthIndex) + 1;
        String selectedMonth = driver.findElements(By.xpath(".//*[@id='luna']/option")).get(randMonth).getText();
        monthSelector.selectByVisibleText(selectedMonth);

        Select yearSelector = new Select(year);
        int yearIndex = driver.findElements(By.xpath(".//*[@id='anul']/option")).size();
        int randYear = rand.nextInt(yearIndex) + 1;
        String selectedYear = driver.findElements(By.xpath(".//*[@id='anul']/option")).get(randYear - 1).getText();
        yearSelector.selectByVisibleText(selectedYear);

        newsletter.click();

        ThreadSleeper.mySleeper(1000L);
        saveChanges.click();
        ThreadSleeper.mySleeper(1000L);

        String actualErrorList = this.errorFullName.getText() + " " + this.errorEmail.getText() + " " + this.errorPhone;
        String expectedErrorList = errorFullName + " " + errorEmail + " " + errorPhone;

        switch (actualErrorList) {
            case "Campul Nume nu poate fi gol." + " " + "Campul Adresa e-Mail nu poate fi gol." + " " + "Campul Telefon 1 nu poate fi gol.": {
                Assert.assertEquals(actualErrorList, expectedErrorList);
                break;
            }
            case "" + " " + "Campul Adresa e-Mail nu poate fi gol." + " " + "Campul Telefon 1 nu poate fi gol.": {
                Assert.assertEquals(actualErrorList, expectedErrorList);
                break;
            }
            case "Campul Nume nu poate fi gol." + " " + "" + " " + "Campul Telefon 1 nu poate fi gol.": {
                Assert.assertEquals(actualErrorList, expectedErrorList);
                break;
            }
            case "Campul Nume nu poate fi gol." + " " + "Campul Adresa e-Mail nu poate fi gol." + " " + "": {
                Assert.assertEquals(actualErrorList, expectedErrorList);
                break;
            }
            case "Campul Nume nu poate fi gol." + " " + "" + " " + "": {
                Assert.assertEquals(actualErrorList, expectedErrorList);
                break;
            }
            case "" + " " + "Campul Adresa e-Mail nu poate fi gol." + " " + "": {
                Assert.assertEquals(actualErrorList, expectedErrorList);
                break;
            }
            case "" + " " + "" + " " + "Campul Telefon 1 nu poate fi gol": {
                Assert.assertEquals(actualErrorList, expectedErrorList);
                break;
            }
            default: {
                break;
            }
        }

    }
}
