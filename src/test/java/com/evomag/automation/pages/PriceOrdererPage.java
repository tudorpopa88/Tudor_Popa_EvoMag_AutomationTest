package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tudor on 11/4/2017.
 */
public class PriceOrdererPage {

    @FindBy(how = How.ID, using = "sortWidget")
    private List<WebElement> sortSelector;

    @FindBy(how = How.XPATH, using = ".//*[@class='real_price']")
    private List<WebElement> productPriceList;

    /**This method is used to sort the products from a page to price ascending order
     * and assert if their prices and indeed in ascending order
     *
     * @param driver is the WebDriver
     */
    public void priceOrdererAsc(WebDriver driver) {
        ThreadSleeper.mySleeper(2000L);
        if (sortSelector.size() == 0) {
            driver.navigate().back();
            CategoryRandomizerPage categoryRandomizer = PageFactory.initElements(driver, CategoryRandomizerPage.class);
            Actions actions = new Actions(driver);
            categoryRandomizer.catRand(actions, driver);
        }
        Select select = new Select(sortSelector.get(0));
        select.selectByValue("PretCrescator");
        ThreadSleeper.mySleeper(2000L);
        List<Double> productPriceListNumericAsc = new ArrayList<Double>();
        for (WebElement product : productPriceList) {
            String product1Asc = product.getText().replace(" Lei", "");
            String product2Asc = product1Asc.replace(".", "");
            String product3Asc = product2Asc.replace(",", ".");
            double product4Asc = Double.parseDouble(product3Asc);
            productPriceListNumericAsc.add(product4Asc);
        }
        System.out.println("The list of prices in ascending order is: ");
        for (int i = 0; i < productPriceListNumericAsc.size(); i++) {
            System.out.println("Product " + (i + 1) + ": " + productPriceListNumericAsc.get(i) + "; ");
        }
        ThreadSleeper.mySleeper(1000L);

        int count = productPriceListNumericAsc.size();
        for (int i = 0; i < count; i++) {
            if (i == count - 1 || productPriceListNumericAsc.get(i) == productPriceListNumericAsc.get(i + 1)) {
                continue;
            }

            try {
                if (i == count - 1) {
                    continue;
                }
                Assert.assertTrue(productPriceListNumericAsc.get(i) < productPriceListNumericAsc.get(i + 1));
            } catch (AssertionError e) {
                System.out.println("One of the products is not in ascending order. Check the order from the console or webpage.");
            }
        }

    }

    /**This method is used to sort the products from a page to price descending order
     * and assert if their prices and indeed in descending order
     *
     * @param driver is the WebDriver
     */
    public void priceOrdererDesc(WebDriver driver) {
        ThreadSleeper.mySleeper(2000L);
        if (sortSelector.size() == 0) {
            driver.navigate().back();
            CategoryRandomizerPage categoryRandomizer = PageFactory.initElements(driver, CategoryRandomizerPage.class);
            Actions actions = new Actions(driver);
            categoryRandomizer.catRand(actions, driver);
        }
        Select select = new Select(sortSelector.get(0));
        select.selectByValue("PretDescrescator");
        ThreadSleeper.mySleeper(2000L);
        List<Double> productPriceListNumericDesc = new ArrayList<Double>();
        for (WebElement product : productPriceList) {
            String product1Desc = product.getText().replace(" Lei", "");
            String product2Desc = product1Desc.replace(".", "");
            String product3Desc = product2Desc.replace(",", ".");
            double product4Desc = Double.parseDouble(product3Desc);
            productPriceListNumericDesc.add(product4Desc);
        }
        System.out.println("The list of prices in descending order is: ");
        for (int i = 0; i < productPriceListNumericDesc.size(); i++) {
            System.out.println("Product " + (i + 1) + ": " + productPriceListNumericDesc.get(i) + "; ");
        }
        ThreadSleeper.mySleeper(1000L);

        int count = productPriceListNumericDesc.size();
        for (int i = 0; i < count; i++) {
            if (i == count - 1 || productPriceListNumericDesc.get(i) == productPriceListNumericDesc.get(i + 1)) {
                continue;
            }

            try {
                if (i == count - 1) {
                    continue;
                }
                Assert.assertTrue(productPriceListNumericDesc.get(i) > productPriceListNumericDesc.get(i + 1));
            } catch (AssertionError e) {
                System.out.println("One of the products is not in descending order. Check the order from the console or webpage.");
            }
        }


    }
}
