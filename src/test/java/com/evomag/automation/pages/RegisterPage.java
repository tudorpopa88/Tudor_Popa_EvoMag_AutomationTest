package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;
import java.util.Random;

public class RegisterPage {

    @FindBy(how = How.ID, using = "RegisterClientForm_Gender_0")
    private WebElement genderFemaleButton;

    @FindBy(how = How.ID, using = "RegisterClientForm_Gender_1")
    private WebElement genderMaleButton;

    @FindBy(how = How.XPATH, using = ".//*[@id='RegisterClientForm_Gender']/input")
    private List<WebElement> genders;

    @FindBy(how = How.ID, using = "RegisterClientForm_FullName")
    private WebElement fullNameField;

    @FindBy(how = How.ID, using = "RegisterClientForm_Email")
    private WebElement emailField;

    @FindBy(how = How.ID, using = "RegisterClientForm_Password")
    private WebElement passwordField;

    @FindBy(how = How.ID, using = "RegisterClientForm_newsletterMe")
    private WebElement newsletterBox;

    @FindBy(how = How.XPATH, using = ".//*[@name='yt0']")
    private WebElement submitButton;

    /**This method is used to test the register page with different test scenarios,
     * assert if the user can register any invalid fullname-email-password combination
     * and assert the error messages combination from the alerts
     * It also randomly switches between the gender type and newsletter availability
     *
     * @param driver is the WebDriver
     * @param name is the first and last name
     * @param email is the email address
     * @param pw is the password
     */
    public void register(WebDriver driver, String name, String email, String pw) {
        int genderCount = genders.size();
        Random rand = new Random();
        int randGender = rand.nextInt(genderCount) + 1;
        genders.get(randGender - 1).click();
        fullNameField.clear();
        fullNameField.sendKeys(name);
        emailField.clear();
        emailField.sendKeys(email);
        passwordField.clear();
        passwordField.sendKeys(pw);
        int randNewsletter = rand.nextInt(2);
        if(randNewsletter == 1) {
            newsletterBox.click();
        }
        ThreadSleeper.mySleeper(2000L);
        submitButton.click();

        ThreadSleeper.mySleeper(1000L);
        Alert alert = driver.switchTo().alert();
        System.out.println(alert.getText());

        switch (alert.getText()) {
            case "Numele trebuie sa contina minim 5 caractere!\n" +
                    "Adresa de e-mail invalida!\n" +
                    "Parola trebuie sa contina minim 5 caractere.": {
                Assert.assertEquals(alert.getText(), "Numele trebuie sa contina minim 5 caractere!\n" +
                        "Adresa de e-mail invalida!\n" +
                        "Parola trebuie sa contina minim 5 caractere.");
                break;
            }
            case "Adresa de e-mail invalida!\n" +
                    "Parola trebuie sa contina minim 5 caractere.": {
                Assert.assertEquals(alert.getText(), "Adresa de e-mail invalida!\n" +
                        "Parola trebuie sa contina minim 5 caractere.");
                break;
            }
            case "Numele trebuie sa contina minim 5 caractere!\n" +
                    "Parola trebuie sa contina minim 5 caractere.": {
                Assert.assertEquals(alert.getText(), "Numele trebuie sa contina minim 5 caractere!\n" +
                        "Parola trebuie sa contina minim 5 caractere.");
                break;
            }
            case "Numele trebuie sa contina minim 5 caractere!\n" +
                    "Adresa de e-mail invalida!": {
                Assert.assertEquals(alert.getText(), "Numele trebuie sa contina minim 5 caractere!\n" +
                        "Adresa de e-mail invalida!");
                break;
            }
            case "Parola trebuie sa contina minim 5 caractere.": {
                Assert.assertEquals(alert.getText(), "Parola trebuie sa contina minim 5 caractere.");
                break;
            }
            default: {
                break;
            }
        }

        alert.accept();

    }
}