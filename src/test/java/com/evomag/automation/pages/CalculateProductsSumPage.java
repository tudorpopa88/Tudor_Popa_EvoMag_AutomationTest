package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by tudor on 11/3/2017.
 */
public class CalculateProductsSumPage {

    @FindBy(how = How.XPATH, using = ".//*[@id='personal_header']/div[1]/div/a/div")
    private WebElement shoppingBasket;

    @FindBy(how = How.XPATH, using = ".//*[@id='sendOrder-form']/table/tbody/tr[*]/td[4]")
    private List<WebElement> prodSum;

    @FindBy(how = How.XPATH, using = ".//*[@id='PretTotal']/b")
    private WebElement totalSum;

    /**This method is used to validate the calculated sum of the currently present products
     * from the shopping basket and compare it with the final payment amount with VAT
     *
     */
    public void calcProdSum() {
        ThreadSleeper.mySleeper(1000L);
        shoppingBasket.click();
        ThreadSleeper.mySleeper(1000L);
        double calculatedProdSum = 0;
        int count = prodSum.size();

        for(int i=0; i<count; i++) {
            String prodSum1 = prodSum.get(i).getText().replace(".", "");
            String prodSum2 = prodSum1.replace(",", ".");
            calculatedProdSum += Double.parseDouble(prodSum2);
            System.out.println(prodSum.get(i).getText());
        }

        DecimalFormat df = new DecimalFormat("#.00");
        String calculatedProdSumFormat = df.format(calculatedProdSum);
        System.out.println("Suma preturilor din cosul de cumparaturi este " + calculatedProdSumFormat);

        String totalSum1 = totalSum.getText().replace(" lei", "");
        String totalSum2 = totalSum1.replace(".", "");
        String totalSumFormat = totalSum2.replace(",", ".");
        System.out.println("Totalul afisat pe pagina este " + totalSumFormat);

        Assert.assertEquals(calculatedProdSumFormat, totalSumFormat);
    }
}
