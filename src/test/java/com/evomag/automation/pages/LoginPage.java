package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;

public class LoginPage {

    @FindBy(how = How.ID, using = "LoginClientForm_Email")
    private WebElement emailPhoneField;

    @FindBy(how = How.ID, using = "LoginClientForm_Password")
    private WebElement passwordField;

    @FindBy(how = How.XPATH, using = ".//*[@name='yt1']")
    private WebElement loginButton;

    @FindBy(how = How.XPATH, using = ".//*[@class='generic_error_message err1']")
    private List<WebElement> generalError;

    /**This method is used to test the login functionality with different test scenarios
     * and assert the error message for invalid data provided and from the alert
     *
     * @param driver is the WebDriver
     * @param ep is the email or phone
     * @param pw is the password
     * @return true if the alert text assertion is true
     */
    public boolean login(WebDriver driver, String ep, String pw) {

        emailPhoneField.clear();
        emailPhoneField.sendKeys(ep);
        passwordField.clear();
        passwordField.sendKeys(pw);
        ThreadSleeper.mySleeper(1000L);
        loginButton.click();

        try {
            Alert alert = driver.switchTo().alert();
            driver.switchTo().alert();
            if (alert.getText().equals("Va rugam sa completati adresa de mail si parola.")) {
                alert.accept();
            } else {
                System.out.println("The text does not match");
            }
            return true;
        } catch (NoAlertPresentException ex) {
            int count = generalError.size();
            if (count > 0) {

                switch (generalError.get(0).getText()) {
                    case "Utilizatorul nu exista, va recomandam sa creati unul nou." : {
                        Assert.assertEquals("Utilizatorul nu exista, va recomandam sa creati unul nou.", generalError.get(0).getText());
                        break;
                    }
                    case "Nu va puteti autentifica! Adresa de email introdusa este invalida!" : {
                        Assert.assertEquals("Nu va puteti autentifica! Adresa de email introdusa este invalida!", generalError.get(0).getText());
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }


            return false;
        }
    }
}
