package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by tudor on 10/29/2017.
 */
public class LoginTestAccountPage {
    @FindBy(how = How.ID, using = "LoginClientForm_Email")
    private WebElement emailPhoneField;

    @FindBy(how = How.ID, using = "LoginClientForm_Password")
    private WebElement passwordField;

    @FindBy(how = How.XPATH, using = ".//*[@name='yt1']")
    private WebElement loginButton;

    /**This method is used to login the test account
     *
     * @param myEmail is the email address
     * @param myPassword is the password
     */
    public void loginTestAcc(String myEmail, String myPassword) {

        emailPhoneField.clear();
        emailPhoneField.sendKeys(myEmail);
        passwordField.clear();
        passwordField.sendKeys(myPassword);
        ThreadSleeper.mySleeper(1000L);
        loginButton.click();

    }
}
