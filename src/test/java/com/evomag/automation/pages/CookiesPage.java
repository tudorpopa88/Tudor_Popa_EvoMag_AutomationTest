package com.evomag.automation.pages;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class CookiesPage {

    /**This method is used to check if a given cookie is present on browser
     *
     * @param driver is the WebDriver
     */
    public void cookExists(WebDriver driver) {
        Cookie c = driver.manage().getCookieNamed("isPushEnabled");
        try {
            Assert.assertEquals("isPushEnabled", c.getName());
        }
        catch (Exception ex) {
            System.out.println("The cookie does not exist");
        }
        System.out.println("Cookie name: " + c.getName());
        System.out.println("Cookie value: " + c.getValue());

    }

    /**This method is used to delete all the cookies and assert if some cookie is still present on browser
     *
     * @param driver is the WebDriver
     */
    public void cookDelete(WebDriver driver) {
        try {
            driver.manage().deleteAllCookies();
            Cookie c = driver.manage().getCookieNamed("PushSubscriberID");
            Assert.assertEquals("PushSubscriberID", c.getName());
        } catch (Exception ex) {
            System.out.println("This cookie does not exist");
        }
    }

}
