package com.evomag.automation.pages;

import com.evomag.automation.utils.ThreadSleeper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by tudor on 10/29/2017.
 */
public class PersonalDetailsPage {
    @FindBy(how = How.XPATH, using = ".//*[@class='account_header']")
    private WebElement accountHeader;

    @FindBy(how = How.XPATH, using = ".//*[@id='personal_header']/div[2]/div[2]/div[1]/a")
    private WebElement accountDetails;

    /**This method is used to navigate to the personal details page
     *
     * @param actions is the Actions interaction
     */
    public void persDetails(Actions actions) {
        ThreadSleeper.mySleeper(2000L);
        actions.moveToElement(accountHeader).build().perform();
        accountDetails.click();

    }
}
